package env

import (
	"github.com/joho/godotenv"
	"log"
)

const (
	user string = "USER"
	db string = "DB"
	password string = "PASSWORD"
	host string = "HOST"
	port string = "PORT"
	encoding string = "ENCODING"
)

func GetEnvParams()map[string]string {
	env, err := godotenv.Read()
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	return map[string]string{
		"user": env[user],
		"dbname": env[db],
		"password": env[password],
		"host": env[host],
		"port": env[port],
		"encoding": env[encoding],
	}
	
}
