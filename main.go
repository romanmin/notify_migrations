package main

import (
	"./db"
	"./migration"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
)

const (
	migrationDir string = "migrations"
	templateName string = "migration.template"
	templateUpFlag string = "/* migration up */"
	templateDownFlag string = "/* migration down */"
	)



func main() {
	db := db.NewDbConnection()


	if len(os.Args[1:]) == 0 {
		log.Println("Missing arguments")
		return
	}
	switch os.Args[1] {
		case "init":
			initDb(db)
		case "-mm", "make-migration":
			makeMigration()
		case "-m", "migrate":
			if len(os.Args)  >= 3 {
				migrationName := os.Args[2]
				config := map[string]string{
					"dir": migrationDir,
					"tName": templateName,
					"upFlag": templateUpFlag,
					"downFlag": templateDownFlag,
				}
				m := migration.NewMigration(&db, migrationName, config)
				action := os.Args[3]
				if action == "up" {
					m.Up()
					log.Println("Migration UP OK")
				} else if action == "down" {
					m.Down()
					log.Println("Migration DOWN OK")
				} else {
					log.Fatalln("Command not found")
				}

			} else {
				log.Fatalln("Need many arguments")
			}

	}

}

func makeTemplate() error {
	data := []byte(fmt.Sprintf("%s\n\n\n\n\n\n\n\n%s", templateUpFlag, templateDownFlag))
	file, err := os.Create(templateName)
	defer file.Close()
	if err != nil {
		log.Println("Unable to create file:", err)
	}
	_, err = file.Write(data)
	if err != nil {
		log.Println("Can't write to template file")
	}

	return err
}


func initDb(db db.DbConn)  {

	err := os.Mkdir(migrationDir, os.ModePerm)
	if err != nil {
		log.Println(err)
	}
	log.Println("Migration dir create OK")
	err = db.InitStorage()
	log.Println("Data base create OK")
	if err != nil {
		log.Println(err)

	}
	err = makeTemplate()
	if err != nil {
		log.Println("Can't create template file")
	}
	log.Println("Template file create OK")

	log.Println("OK")
}

func makeMigration()  {
	if len(os.Args) < 3 {
		log.Println("Missing migration name")
	}

	if len(os.Args) > 3 {
		log.Println("Too many arguments")
	}
	t := time.Now()
	currentTimeString := fmt.Sprintf("%d%d%d%d%d%d", t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), t.Second())

	migrationName := fmt.Sprintf("%s_%s", currentTimeString, os.Args[2])

	templatefile, err :=  os.Open(templateName)
	defer templatefile.Close()

	b, err := ioutil.ReadAll(templatefile)

	if err != nil {
		log.Fatalln("Can't read template file")
	}
	transactionFile, err := os.Create(fmt.Sprintf("%s/%s.sql", migrationDir, migrationName))

	defer transactionFile.Close()
	if err != nil{
		fmt.Println("Unable to create file:", err)
		os.Exit(1)
	}
	transactionFile.Write(b)


	log.Println("DONE")

}





