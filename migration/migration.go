package migration

import (
	"bufio"
	"fmt"
	"../db"
	"log"
	"os"
	"path/filepath"
)
type migration struct {
	name string
	Db *db.DbConn
	upQuery string
	downQuery string
	config map[string]string

}

func NewMigration(db *db.DbConn, name string, config map[string]string) migration {
	return migration {
		name: name,
		Db: db,
		config: config,
	}
}

func (m *migration) Up() {
	m.readMigrationFile()
	err := m.Db.Query(m.upQuery)
	if err != nil {
		log.Fatalln(err)
	}
}

func (m *migration) Down()  {
	m.readMigrationFile()
	err := m.Db.Query(m.downQuery)
	if err != nil {
		log.Fatalln(err)
	}
}


func (m *migration) readMigrationFile()  {
	files, _ :=  filepath.Glob(fmt.Sprintf("%s/*_%s.sql",m.config["dir"], m.name))

	transactionFile, err :=  os.Open(files[0])
	defer transactionFile.Close()

	scanner := bufio.NewScanner(transactionFile)
	for scanner.Scan() {
		if scanner.Text() == m.config["upFlag"] {
			continue
		}
		if scanner.Text() == m.config["downFlag"] {
			for scanner.Scan() {
				m.downQuery += scanner.Text() + "\n"
			}
			break
		}
		m.upQuery += scanner.Text() + "\n"

	}

	if err = scanner.Err(); err != nil {
		log.Fatal(err)
	}


}