Зависимости 


1. `github.com/lib/pq` 
2. `github.com/joho/godotenv`

Как рабоает:
1. Вводим в .env конфигаруацию дб
2. Билдим
3. Вызваем **./main init** для создание шаблонных файлов и папок, проверки подключения к дб и тд

Команды:
1. **makemigration (-mm) [name]** - создает файл миграции в папке migrations
2. **magrate (-m) [name] [flag]** - аппает или дропает миграцию 

Пример:
* **./main -mm CreateAuthTable**
* **./main -m CreateAuthTable up**


Синтаксис в migrations/*.sql
Тут все просто:
* Все что идет после `/* migration up */` будует отрабатывать при апе миграции 
* Все что идет после `/* migration down */` будует отрабатывать при апе миграции 
