package db

import (
	"../env"
	"fmt"
	_ "github.com/lib/pq"
	"database/sql"
	"log"
)

type DbConn struct {
	dbConfig map[string]string
	db sql.DB
}

func NewDbConnection() DbConn {
	dbConfigFromEnv := env.GetEnvParams()
	return DbConn {
		dbConfig:dbConfigFromEnv,
	}

}

func (d *DbConn) connect() error {
	connStr := fmt.Sprintf("user=%s password=%s dbname=%s", d.dbConfig["user"], d.dbConfig["password"], d.dbConfig["dbname"])
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatalln(err)
	}
	d.db = *db
	return err
}

func (d *DbConn) InitStorage() error {
	connStr := fmt.Sprintf("user=%s password=%s", d.dbConfig["user"], d.dbConfig["password"])
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatalln(err)
	}
	_, err = db.Query(fmt.Sprintf(`CREATE DATABASE %s WITH ENCODING  = '%s'`, d.dbConfig["dbname"], d.dbConfig["encoding"]))

	return err
}

func (d *DbConn) Query(query string) error  {
	fmt.Println(query)
	err := d.connect()
	_, err = d.db.Query(query)

	return err
}

